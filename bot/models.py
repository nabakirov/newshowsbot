from django.db import models

class user(models.Model):
    user_id = models.IntegerField(default=0)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=50)

    def __str__(self):
        return self.first_name

class serial(models.Model):
    title = models.CharField(max_length=100)
    def __str__(self):
        return self.title


class users_by_serial(models.Model):
    serial = models.ForeignKey(serial, related_name='users')
    user_id = models.IntegerField()
    notified = models.BooleanField(default = False)
      
    def __str__(self):
        return self.serial.title


class notified_episode(models.Model):
    
    serial = models.ForeignKey(users_by_serial, related_name='episodes', default = '')
    episode = models.CharField(max_length = 200)




