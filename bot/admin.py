from django.contrib import admin
from .models import users_by_serial, user, serial


admin.site.register(user)
admin.site.register(users_by_serial)
admin.site.register(serial)
