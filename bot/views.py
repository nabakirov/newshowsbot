from django.shortcuts import render
from django.http import JsonResponse
import json
import telebot
from .STATIC import token, admin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.decorators import method_decorator
from .commads import start, update, list_of_serials, add_serial, Help, reset, userscnt, userslist, serialscnt, serialslist
from .models import user









bot = telebot.TeleBot(token)


class CommandReceiveView(View):
    def post(self, request):



            raw = request.body.decode('utf-8')
            commands = {
                '/start': start,
                '/list': 'serials',
                '/help': Help,

            }
            admin_commadns = {
                '/start': start,
                '/list': 'serials',
                '/help': Help,
                'reset': reset,
                'update': update,
                'userscnt': userscnt,
                'userslist': userslist,
                'serialscnt': serialscnt,
                'serialslist': serialslist,
            }

            try:
                payload = json.loads(raw)
            except ValueError:
                print('json error')
            else:
                chat_id = payload['message']['chat']['id']
                cmd = payload['message'].get('text')
                user_id = payload['message']['from']['id']
                try:
                    username = payload['message']['from']['username']
                    last_name = payload['message']['from']['last_name']
                except:
                    username = '-'
                    last_name = '-'
                first_name = payload['message']['from']['first_name']

                try:
                    check = user.objects.get(user_id=user_id)
                except:
                    check = False
                if not check:
                    new_user = user()
                    new_user.user_id = user_id
                    new_user.first_name = first_name
                    new_user.last_name = last_name
                    new_user.username = username
                    new_user.save()
                if int(chat_id) == int(admin):
                    func = admin_commadns.get(cmd.split()[0].lower())
                else:
                    func = commands.get(cmd.split()[0].lower())
                if func:
                    if func == 'serials':
                        bot.send_message(chat_id, list_of_serials(user_id))

                    else:
                        bot.send_message(chat_id, func())
                else:
                    bot.send_message(chat_id, add_serial(user_id, cmd))


            return JsonResponse({}, status=200)


    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CommandReceiveView, self).dispatch(request, *args, **kwargs)
