# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-06-13 06:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0005_auto_20170612_1734'),
    ]

    operations = [
        migrations.AddField(
            model_name='notified_episode',
            name='serial',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='episodes', to='bot.users_by_serial'),
        ),
        migrations.AlterField(
            model_name='notified_episode',
            name='episode',
            field=models.CharField(max_length=200),
        ),
    ]
