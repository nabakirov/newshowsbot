from django.http import HttpResponse
from django.template.loader import render_to_string
from .update import get_news, get_html, reset_notifications
from .models import serial, user
from .models import users_by_serial




def start():

    return render_to_string('help.md')
def Help():
    return render_to_string('help.md')



def list_of_serials(user_id):

    try:
        serials = users_by_serial.objects.filter(user_id=user_id)
    except:
        serials = False


    if serials:

        return render_to_string('serials_list.md', {'items': serials})
    else: return render_to_string('empty_list.md')


def add_serial(id, title):
    if search(title):
        title = search(title)
        try:
            target = serial.objects.get(title=title)

        except:
            target = False

        if not target:
            to_add = serial(title=title)
            to_add.save()

            to_add_user = users_by_serial(user_id=id,
                                          serial=to_add)
            to_add_user.save()

            return render_to_string('add_new.md', {'title': title})
        else:
            try:
                new = users_by_serial.objects.get(serial=target, user_id=id)
            except:
                new = False

            if new:
                new.delete()
                try:
                    ser_to_del = target.users.all()
                except:
                    ser_to_del = False
                if not ser_to_del:
                    target.delete()

                return render_to_string('remove.md', {'title': title})
            else:
                to_add_user = users_by_serial(user_id=id,
                                              serial=target)
                to_add_user.save()

                return render_to_string('add_new.md', {'title': title})
    else:

        return render_to_string('search_error.md')



from bs4 import BeautifulSoup

toDel = ['[', ']', "'", ',', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '=']
def search(title):
    title = title.translate({ord(c): '' for c in toDel})
    title = title.translate({ord(' '): '+'}).lower()
    url = 'http://seasonvar.ru/search?q=' + title
    html = get_html(url)
    soup = BeautifulSoup(html, 'lxml')

    try:
        new_title = soup.find('div', class_="pgs-search-info")
    except:
        return False

    if new_title:
        new_title = str(new_title.find('a').string)
        return new_title


def updateTimer(request):
    ans = get_news()
    return HttpResponse('update')

def resetTimer(request):
    reset_notifications()
    return HttpResponse('reset')

# admin commands

def update():
    get_news()
    return 'updated'
def reset():
    reset_notifications()
    return 'reset'

def userscnt():
    users = user.objects.count()
    return users
def serialscnt():
    serials = serial.objects.count()
    return serials
def serialslist():
    serlist = serial.objects.all()
    return render_to_string('serlist.md', {'serlist': serlist})

def userslist():
    users = user.objects.all()
    return render_to_string('userslist.md', {'users': users})







