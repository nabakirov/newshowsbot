import requests
from bs4 import BeautifulSoup
from datetime import datetime
from .STATIC import URL, token
import telebot
from .models import users_by_serial, serial, notified_episode
from django.template.loader import render_to_string


bot = telebot.TeleBot(token)


class data:
    def __init__(self, title='', episode='', download_link='', watch_link='', notified=False):
        self.title = title
        self.episode = episode
        self.download_link = download_link
        self.watch_link = watch_link
        self.notified = notified


def get_html(url):
    response = requests.get(url)
    return response.text


def get_download_link(url):
    download_link = '-'

    return download_link


def get_news():
    html = get_html(URL)

    soup = BeautifulSoup(html, 'lxml')
    now = datetime.now()

    all_news = soup.find_all('div', class_="news")
    today = now.strftime("%d.%m.%Y")
    '''
    %d is the day number
    %m is the month number
    %b is the month abbreviation
    %y is the year last two digits
    %Y is the all year
    '''
    toDel = ['[', ']', "'", ',']
    rawDate = str(all_news[0].find('div', class_="news-head").string.split())
    dateToCheck = rawDate.translate({ord(c): '' for c in toDel})

    if dateToCheck == today:
        serials = all_news[0].find_all('a')
        for seriala in serials:
            item = data()
            item.title = str(seriala.find('div', class_="news_n").string.split()).translate({ord(c): '' for c in toDel})
            item.watch_link = URL + str(seriala.get('href'))
            item.download_link = '-'
            season = str(seriala.find('div', class_="news-w").text).split()
            item.episode = str(season).translate({ord(c): '' for c in toDel}).replace('{}'.format(item.title), '')
            
            try:
                ser = serial.objects.get(title = item.title)

            except:
                ser = False

            if ser:

                try:
                    ID = ser.users.all()
                except:
                    ID = False

                if ID:
                    for user in ID:
                        try:
                            episodes = user.episodes.get(episode = item.episode)
                        except:
                            episodes = False
                        
                        if not episodes:
                            try:
                                bot.send_message(user.user_id, message(item))

                                aa = notified_episode(serial = user, episode = item.episode)
                                aa.save()
                                user.save()
                            except:
                                print('User has delete the chat')




def message(item):
    return render_to_string('feed.md', {'items': item})

def reset_notifications():
    users = notified_episode.objects.all().delete()
    return 'reset'



